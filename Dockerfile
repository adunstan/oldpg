FROM fedora:28

# set these for the build if you want a more restrictive hba line
ARG localnet=0.0.0.0
ARG localmask=0.0.0.0

COPY libreadline.so.4.3 libtermcap.so.2.0.8 libnsl-2.3.3.so /usr/lib64/
RUN cd /usr/lib64 && \
	ln -s libreadline.so.4.3 libreadline.so.4 && \
	ln -s libtermcap.so.2.0.8 libtermcap.so.2.0 && \
	ln -s libtermcap.so.2.0 libtermcap.so.2 && \
	ln -s libnsl-2.3.3.so libnsl.so.1

RUN groupadd -g 26 -o -r postgres >/dev/null 2>&1 || :
RUN useradd -M -g postgres -o -r -d /var/lib/pgsql -s /bin/bash \
        -c "PostgreSQL Server" -u 26 postgres >/dev/null 2>&1 || :

WORKDIR /app

RUN chown postgres:postgres /app

USER postgres

COPY --chown=postgres:postgres pg728bin.tgz /app

RUN tar -z --strip-components=3 -xf pg728bin.tgz && rm pg728bin.tgz

WORKDIR /app/REL7_2_STABLE/inst

ENV LD_LIBRARY_PATH=/app/REL7_2_STABLE/inst/lib

RUN echo 'tcpip_socket = true' >> data-C/postgresql.conf
RUN echo host all ${localnet} ${localmask} trust >> data-C/pg_hba.conf

RUN bin/pg_ctl -D data-C -l logfile start && \
	sleep 5 && \
	bin/psql -U andrew -c 'create user postgres createdb createuser' \
			 template1 && \
	bin/psql -c 'create database postgres' template1 && \
	bin/pg_ctl -D data-C -l logfile -w stop

EXPOSE 5432/tcp
# run the postmaster in the foreground
CMD bin/postmaster -D data-C >> logfile 2>&1

