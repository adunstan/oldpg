OldPG
=====

This is a Vagrant setup that installs and runs old PostgreSQL
(initially version 7.2.8) on a modern OS (here Fedora 28). The motivation
for this is to test that pg_dump works with old versions.

This has been tested against a `virtualbox` provider for Vagrant, but there
is no reason it shouldn't work with small adjustmnents on other providers
such as `libvirt` or even `aws`.

The binaries and data directory were built on a Fedora Core 2 VM. They
require a few libraries from FC2 that are no longer present on Fedora 28.
These are installed by the Vagrant scripts.
The regression database from a `make installcheck` run is present for
testing `pg_dump` against.

If you try to connect using a modern `psql`, you will need to set the client
encoding via the `PGCLIENTENCODING` environment variable.

`pg_dump` and other programs from any version up to 9.6 should work against
very old versions. Later versions no longer support the old wire protocol, and
won't work against any version less than 8.0.

