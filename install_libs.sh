#!/bin/sh

set -e

# disable the firewall - could also just allow access on the postgres ports
systemctl stop firewalld
systemctl disable firewalld

# backwards compatibility libraries needed for postgres binaries from FC2
cp /vagrant/libreadline.so.4.3 /vagrant/libtermcap.so.2.0.8 /usr/lib64
cp /vagrant/libnsl-2.3.3.so /usr/lib64
cd /usr/lib64
ln -s libreadline.so.4.3 libreadline.so.4
ln -s libtermcap.so.2.0.8 libtermcap.so.2.0
ln -s libtermcap.so.2.0 libtermcap.so.2
ln -s libnsl-2.3.3.so libnsl.so.1
