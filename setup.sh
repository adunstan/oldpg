#!/bin/sh

localnet=$1

set -e

# unpack binaries and data directory built on FC2
tar -z --strip-components=3 -xf /vagrant/pg728bin.tgz

cd REL7_2_STABLE/inst/data-C

# set up TCP networking
echo 'tcpip_socket = true' >> postgresql.conf
# trust the local network
echo "host all $localnet 255.255.255.0 trust">> pg_hba.conf

# add a postgres user and database
cd ..
export LD_LIBRARY_PATH=`pwd`/lib
# -w seems to be broken :-(
bin/pg_ctl -D data-C -l logfile start
sleep 5
bin/psql -U andrew -c 'create user postgres createdb createuser' template1
bin/psql -U postgres -c 'create database postgres' template1
bin/pg_ctl -D data-C -l logfile -w stop






