#!/bin/sh

## script to run every time the machine is booted.
## Just start the postgres instance(s)

set -e

cd REL7_2_STABLE/inst
# so we can find libpq
export LD_LIBRARY_PATH=`pwd`/lib
# don't use -w
bin/pg_ctl -D data-C -l logfile start
sleep 5

